# shellcheck shell=sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

for p in \
		"$HOME/.local/bin"; do
	case :$PATH:
		in *:$p:*) ;;
		*) [ -d "$p" ] && export PATH="$p:$PATH" ;;
	esac
done
