# shellcheck shell=sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if [ -z "$PREFIX" ]; then
	if [ -z "$TERMUX_PREFIX" ]; then
		export TERMUX_PREFIX=/data/data/com.termux/files/usr
	fi
	export PREFIX="$TERMUX_PREFIX"
elif [ -z "$TERMUX_PREFIX" ]; then
	export TERMUX_PREFIX="$PREFIX"
fi

export XDG_DATA_DIRS="$TERMUX_PREFIX/share"
export XDG_CONFIG_DIRS="$TERMUX_PREFIX/etc/xdg"
