# shellcheck shell=sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

[ -n "$XDG_DATA_HOME" ] || export XDG_DATA_HOME="$HOME/.local/share"
[ -n "$XDG_CONFIG_HOME" ] || export XDG_CONFIG_HOME="$HOME/.config"
[ -n "$XDG_STATE_HOME" ] || export XDG_STATE_HOME="$HOME/.local/state"
[ -n "$XDG_DATA_DIRS" ] || export XDG_DATA_DIRS="/usr/local/share:/usr/share"
[ -n "$XDG_CONFIG_DIRS" ] || export XDG_CONFIG_DIRS="/etc/xdg"
[ -n "$XDG_CACHE_HOME" ] || export XDG_CACHE_HOME="$HOME/.cache"
