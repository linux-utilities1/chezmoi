# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-login
	exit
end

if test -z "$PREFIX"
	if test -z "$TERMUX_PREFIX"
		set -Ux TERMUX_PREFIX /data/data/com.termux/files/usr
	end
	set -Ux PREFIX $TERMUX_PREFIX
else if test -z "$TERMUX_PREFIX"
	set -Ux TERMUX_PREFIX $PREFIX
end

set -Ux --path XDG_DATA_DIRS $TERMUX_PREFIX/share
set -Ux --path XDG_CONFIG_DIRS $TERMUX_PREFIX/etc/xdg
