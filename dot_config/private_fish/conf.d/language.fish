# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-login
	exit
end

set --export LANGUAGE en_CA.UTF-8:en_GB.UTF-8:en_AU.UTF-8:en.UTF-8:en_US.UTF-8
set --export LANG en_CA.UTF-8
set --export LC_COLLATE en_CA.UTF-8
set --export LC_CTYPE en_CA.UTF-8
set --export LC_MESSAGES en_CA.UTF-8
set --export LC_MONETARY en_CA.UTF-8
set --export LC_NUMERIC en_CA.UTF-8
set --export LC_TIME en_CA.UTF-8
