# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-login
	exit
end

set --export STEAM_FORCE_DESKTOPUI_SCALING 2
