# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-login
	exit
end

for p in \
		.local/bin \
		Applications/platform-tools \
		repos/rsync-incremental-backup
	set --local hp $HOME/$p
	if test -d $hp; and not contains $hp $PATH
		set --export PATH $hp $PATH
	end
end
