# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-login
	exit
end

if test -z "$XDG_DATA_HOME"; set -Ux XDG_DATA_HOME $HOME/.local/share; end
if test -z "$XDG_CONFIG_HOME"; set -Ux XDG_CONFIG_HOME $HOME/.config; end
if test -z "$XDG_STATE_HOME"; set -Ux XDG_STATE_HOME $HOME/.local/state; end
if test -z "$XDG_DATA_DIRS"; set -Ux --path XDG_DATA_DIRS /usr/local/share:/usr/share; end
if test -z "$XDG_CONFIG_DIRS"; set -Ux --path XDG_CONFIG_DIRS /etc/xdg; end
if test -z "$XDG_CACHE_HOME"; set -Ux XDG_CACHE_HOME $HOME/.cache; end
