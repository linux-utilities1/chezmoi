# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if not status is-interactive
	exit
end

bind -e \e\[C; and bind \e\[C forward-single-char # right arrow
bind \e\[3\;5~ kill-word # Ctrl-Delete
bind \b backward-kill-word # Ctrl-Backspace
