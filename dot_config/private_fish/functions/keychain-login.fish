# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

function keychain-login
	set -g keyChainLoginIsQuiet "false"
	if set -q argv[1]; and test \( $argv[1] = "-q" -o $argv[1] = "--quiet" \)
		set keyChainLoginIsQuiet "true"
	end
	function say
		if test $keyChainLoginIsQuiet = "true"
			return
		end
		echo $argv[1]
	end
	function cleanup
		set -e keyChainLoginIsQuiet
	end

	function check_command
		if not command --query $argv[1]
			say "$argv[1] is not installed." >&2
		end
	end
	for cmd in keychain ssh-add
		if not check_command $cmd
			cleanup; return 1
		end
	end

	set keys
	# These are the keys that ssh-agent checks for
	for f in ~/.ssh/id_{rsa,ecdsa,ecdsa_sk,ed25519,ed25519_sk,id_dsa}
		if test -f $f -a -r $f
			set --append keys $f
		else if test -L $f
			set -l rf (builtin realpath $f)
			if test -f $rf -a -r $rf
				set --append keys $f
			end
		end
	end

	SHELL=(status fish-path) keychain --timeout 120 --eval --quiet --quick $keys | source
	if ssh-add -l -q >/dev/null
		say "Success."
	else
		say "Failure."
		cleanup; return 2
	end
	cleanup
end
