# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

function rsib --description "Alias for rsync-incremental-backup-local"
	if not command --query rsync-incremental-backup-local
		echo "Couldn't find rsync-incremental-backup-local in PATH" >&2
		return 100
	end
	set --local depth 100
	backupDepth=$depth maxLogFiles=$depth rsync-incremental-backup-local $argv
	return $status
end
