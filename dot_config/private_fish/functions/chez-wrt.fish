# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

function chez-wrt
	function log_write
		echo "[chez-wrt] $argv[1]"
	end
	function log_fatal
		log_write "FATAL: $argv[1]" >&2
	end
	function log_err
		log_write "ERROR: $argv[1]" >&2
	end

	for cmd in chezmoi mktemp
		if not command -q $cmd
			log_fatal "$cmd was not found in PATH."
			return 3
		end
	end

	set lArg (count $argv)
	if test $lArg -lt 1 -o $lArg -gt 2
		set -l details "In the first form, the username defaults to 'root'. In the second form, you may"
		set --append details "(attempt to) use usernames and hosts that contain the @ character."
		echo "Usage:" >&2
		echo "chez-wrt [username@]host" >&2
		echo "chez-wrt username host" >&2
		echo "$details" >&2
		return 2
	end

	if test $lArg -eq 2
		set user $argv[1]
		set host $argv[2]
	else
		if not string match --regex --groups-only --quiet \
				'^(?:(?<user>[^@]+)@)?(?<host>[^@]+)$' $argv[1]
			set -l msg "If calling with one argument, then that argument must be of the form"
			set --append msg "[username@]host. The default username is 'root'."
			log_fatal "$msg"
			return 2
		end
		set -q user[1]; or set user "root"
	end
	# Now user and host are set.
	
	if not builtin functions --query keychain-login
		log_err "keychain-login function is not present."
	else if not keychain-login -q
		log_err "Failed to add key using keychain-login."
	end

	if not set workdir (mktemp -d)
		log_fatal "Failed to create a temporary directory to work in."
		return 3
	end

	# TODO mount remote home folder, apply, unmount
	#if not echo '{"data":{"isopenwrt":true}}' | \
	#		chezmoi --config /dev/stdin --config-format json 
	#	
	#end

end
