# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

function git-pushssh
	function log_write
		set -q argv[1]; or set argv[1] ""
		echo "[git-pushssh] $argv[1]"
	end
	function log_info
		set -q argv[1]; or set argv[1] ""
		log_write "INFO: $argv[1]"
	end
	function log_warn
		set -q argv[1]; or set argv[1] ""
		log_write "WARNING: $argv[1]" >&2
	end
	function log_fatal
		set -q argv[1]; or set argv[1] ""
		log_write "FATAL: $argv[1]" >&2
	end
	command -q git; or begin; log_fatal "No git in PATH."; return 2; end
	set rems (git remote); or return 1 # Covers not-a-git-repo
	set numRems (count $rems)
	if test $numRems -eq 0
		log_fatal "This repo has no remotes."; return 3
	else if test $numRems -gt 1; and not set -q argv[1]
		set -l msg "This repo has multiple remotes, so you must specify one:"
		set --append msg "$(string join ", " $rems)."
		log_fatal "$msg"; return 4
	else if set -q argv[1]; and not contains $argv[1] $rems
		set -l msg
		if test $numRems -gt 1
			set msg "This repo has no remote called '$argv[1]'. Its remotes are:"
			set --append msg "$(string join ", " $rems)."
		else
			set msg "This repo has one remote and it is called '$rems', not '$argv[1]'."
		end
		log_fatal "$msg"; return 5
	end
	# Picture a 3x3 grid of possibilities. Number of existing remotes (0, 1, more than 1) against
	# the nature of $argv[1] (none, matching an existing remote, not matching an existing remote).
	# At this point, the only remaining possibilities are:
	# 1 remote, no argument
	# 1 remote, matching argument
	# 2+ remotes, matching argument
	set -q argv[1]; or set argv $rems # Now all remaining possibilities involve a matching argument

	set pullUrls (git remote get-url --all $argv[1]); and \
	set pushUrls (git remote get-url --push --all $argv[1]); and \
	for url in $pullUrls
		if not string match --groups-only --regex --quiet \
				'^https?://(?<uDomain>[^/]+)/(?<uPlace>.+)$' $url
			log_warn "Skipping non-matching URL: $url"
			continue
		end
		set -l newUrl "git@$uDomain:$uPlace"
		if contains $newUrl $pushUrls
			log_warn "Skipping adding push URL to remote '$argv[1]' because it already exists: $newUrl"
			continue
		end
		if not git remote set-url --add --push $argv[1] $newUrl
			log_warn "Couldn't add push URL to remote '$argv[1]': $newUrl"
			continue
		end
		log_info "Added push URL to remote '$argv[1]': $newUrl"
	end
end
