# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2024 Adam Beer

function install-fisher
	if not test command -q curl
		echo "FATAL: curl is not in PATH." >&2
		return 1
	end
	curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish \
		| source; and fisher update
end
