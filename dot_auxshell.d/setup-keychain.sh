# shellcheck shell=sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

# Substitute for Keychain, https://www.funtoo.org/Funtoo:Keychain

setup_keychain() {
	for cmd in ssh-agent ssh-add; do
		if ! command -v "$cmd" >/dev/null; then
			>&2 echo "$cmd is not in PATH."
			exit 2
		fi
	done

	if [ -n "${SSH_AUTH_SOCK:-}" ] && [ -n "${SSH_AGENT_PID:-}" ]; then
		echo "The agent appears to be running."
	elif [ -z "${SSH_AUTH_SOCK:-}" ] && [ -z "${SSH_AGENT_PID:-}" ]; then
		echo "Starting agent."
		if ! agent_text=$(ssh-agent -t 2h) || ! eval "$agent_text"; then
			>&2 echo "Failed to start agent."; exit 3
		fi
	else
		>&2 echo "Only one variable is set."
		>&2 echo "SSH_AUTH_SOCK: $SSH_AUTH_SOCK"
		>&2 echo "SSH_AGENT_PID: $SSH_AGENT_PID"
		exit 4
	fi
	ssh-add
}
