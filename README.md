 <!-- SPDX-License-Identifier: CC-BY-NC-SA-4.0 -->
 <!-- © 2023 Adam Beer -->

# chezmoi

This repo is for use with [chezmoi](https://chezmoi.io/).

# Notes

## LibreWolf Portable (Windows)
To bootstrap: in the `Applications/librewolf-portable` folder, make sure that:

- `LibreWolf-Portable.exe` exists
- `LibreWolf-WinUpdater.exe` exists
- `LibreWolf-WinUpdater.ini` does not exist
- In the `LibreWolf` subfolder, an out-of-date version of `librewolf.exe` exists

Total size: ~1.2MiB

Then run `LibreWolf-Portable.exe`.
