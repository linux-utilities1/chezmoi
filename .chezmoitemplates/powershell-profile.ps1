# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

$expectedFiles = @('plugin-chezmoi.ps1', 'plugin-choco.ps1', 'plugin-gsudo.ps1')

foreach ($name in $expectedFiles) {
  $path = Join-Path $PSScriptRoot $name
  if (Test-Path $path -PathType Leaf) {
    . $path
  }
}
