# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2025 Adam Beer

# TODO Investigate whether I'll ever be with git but without starship (I think only /possibly/ Windows)
# and if not, delete this file and the two mentioned below.
if command -v git >/dev/null && ! command -v starship >/dev/null; then
	if ! command -v __git_find_repo_path >/dev/null; then
		# shellcheck source=../dot_auxshell.d/git-completion.bash
		[ -r ~/.auxshell.d/git-completion.bash ] && . ~/.auxshell.d/git-completion.bash
	fi
	if ! command -v __git_ps1 >/dev/null; then
		# shellcheck source=../dot_auxshell.d/git-prompt.bash
		[ -r ~/.auxshell.d/git-prompt.bash ] && . ~/.auxshell.d/git-prompt.bash
	fi
fi
