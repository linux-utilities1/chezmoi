# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

shopt -s \
	autocd \
	cdspell \
	globstar \
	
export HISTSIZE=2000
export HISTFILESIZE=2000
