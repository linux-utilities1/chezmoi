# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2025 Adam Beer

# TODO Investigate whether I'll ever be with git but without starship (I think only /possibly/ Windows)
# and delete this file if not
if ! command -v starship >/dev/null; then
	export PROMPT_DIRTRIM=3

	export GIT_PS1_SHOWDIRTYSTATE=1
	export GIT_PS1_SHOWSTASHSTATE=1
	export GIT_PS1_SHOWUNTRACKEDFILES=1
	export GIT_PS1_SHOWUPSTREAM=verbose

	# shellcheck disable=SC2154
	case $MSYSTEM in
		MINGW*)
			if [ -f /etc/profile.d/git-sdk.sh ]; then
				TITLEPREFIX=SDK-${MSYSTEM#MINGW}
			else
				TITLEPREFIX=$MSYSTEM
			fi
			PS1='\[\033]0;$TITLEPREFIX:$PWD\007\]' ;;
		*)
			PS1='\[\033[35m\]\u\[\033[34m\]@\[\033[33m\]\h\[\033[34;1m\]:\[\033[0m\]' ;;
	esac

	PS1=$PS1'\[\033[32m\]\w'

	# shellcheck disable=SC2154
	case $MSYSTEM in
		MINGW*)
			if [ -z "$WINELOADERNOEXEC" ] && command -v git >/dev/null; then
				GIT_EXEC_PATH="$(git --exec-path 2>/dev/null)"
				COMPLETION_PATH="${GIT_EXEC_PATH%/libexec/git-core}"
				COMPLETION_PATH="${COMPLETION_PATH%/lib/git-core}"
				COMPLETION_PATH="$COMPLETION_PATH/share/git/completion"

				# See 10-source-git-stuff.bash
				if ! command -v __git_find_repo_path >/dev/null \
						&& [ -r "$COMPLETION_PATH/git-completion.bash" ]; then
					# shellcheck source="C:\Program Files\Git\mingw64\share\git\completion\git-completion.bash"
					. "$COMPLETION_PATH/git-completion.bash"
				fi
				if ! command -v __git_ps1 >/dev/null \
						&& [ -r "$COMPLETION_PATH/git-prompt.bash" ]; then
					# shellcheck source="C:\Program Files\Git\mingw64\share\git\completion\git-prompt.sh"
					. "$COMPLETION_PATH/git-prompt.sh"
				fi
			fi ;;
		*) ;;
	esac
	if command -v __git_ps1 >/dev/null; then
		PS1=$PS1'\[\033[36m\]$(__git_ps1 "{%s}")'
	fi

	# shellcheck disable=SC2154
	PS1=$PS1'\[\033[35m\]$(nj="\j"; [ $nj -gt 0 ] && echo ${nj}J)\[\033[34;1m\]\$\[\033[0m\] '

	# shellcheck disable=SC2154
	case $MSYSTEM in
		MINGW*)
			# shellcheck disable=SC2034
			MSYS2_PS1="$PS1" ;;
		*) ;;
	esac
fi
