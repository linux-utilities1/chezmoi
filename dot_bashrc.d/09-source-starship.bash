# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2025 Adam Beer

if command -v starship >/dev/null; then
	eval "$(starship init bash)"
else
	>&2 echo "Starship was not found in PATH."
fi
