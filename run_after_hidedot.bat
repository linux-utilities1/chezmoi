@ECHO OFF
REM SPDX-License-Identifier: AGPL-3.0-or-later
REM © 2023 Adam Beer

FOR %%F IN (.bash_profile .bashrc .profile .shrc ^
.auxshell.d .bashrc.d .config .profile.d .shrc.d) DO ATTRIB +H %%F
