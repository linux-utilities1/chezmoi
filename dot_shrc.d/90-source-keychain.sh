# shellcheck shell=sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# © 2023 Adam Beer

if ! command -v keychain >/dev/null && ! command -v setup_keychain >/dev/null; then
	# shellcheck source=../dot_auxshell.d/setup-keychain.sh
	[ -r ~/.auxshell.d/setup-keychain.sh ] && . ~/.auxshell.d/setup-keychain.sh
fi
